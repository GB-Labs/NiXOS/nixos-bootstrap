# (Simple) Bootstrap to NixOS #

This is a little project to showcase how to bootstrap a NixOS machine using a BTRFS flesystem - with swap space - on a *single disk* (no RAID).

## How to use ##

Boot your machine using the installation CD, see [NixOS Download page](https://nixos.org/nixos/download.html); I suggest using the ``Minimal Installation CD``.

Install the (minimal) ``git`` package: ``nix-env -iA nixos.gitMinimal`` and clone this repo on your machine: ``git clone https://gitlab.com/GB-Labs/NiXOS/nixos-bootstrap.git``.

**WARNING**: be sure to replace the ssh public key with *yours* in ``nixos-bootstrap/ssh-keys.nix``.

Customize the ``device`` variable in ``nixos-bootstrap/bootstrap.sh`` using the device of your choice, use ``lsblk`` to list available devices.

Start the bootstrap process: ``cd nixos-bootstrap && sh ./bootstrap.sh``

When done with the configuration, install NixOS ``nixos-install`` and if all went well ``reboot``.

